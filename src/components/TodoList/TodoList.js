import ClickOutside from 'vue-click-outside';

export default {
  name: 'TodoList',
  data() {
    return {
      storage: {
        key: 'vue-todo',
        uid: 0,
      },
      todos: [],
      availableColor: [
        '#ef5350',
        '#f06292',
        '#7c57c2',
        '#ffe082',
      ],
      selectedColor: '#ef5350',
      newTodo: '',
      newTodoError: false,
      editedTodo: null,
      cachedEditTodo: null,
    };
  },
  watch: {
    todos: {
      handler(todos) {
        localStorage.setItem(this.storage.key, JSON.stringify(todos));
      },
      deep: true,
    },
  },
  methods: {
    selectColor(color) {
      this.selectedColor = color;
    },
    addTodo() {
      const name = this.newTodo && this.newTodo.trim();
      const color = this.selectedColor;

      if (!name) {
        this.newTodoError = true;

        return;
      }

      this.todos.push({
        id: this.storage.uid++,
        completed: false,
        editMode: false,
        color,
        name,
      });

      this.setDefault();
    },
    editTodo(todo) {
      if (todo.completed) return;

      if (this.editedTodo && !(this.editedTodo === todo)) {
        this.cancelEdit(this.editedTodo);
      }

      if (!(this.editedTodo === todo)) {
        this.editedTodo = todo;

        this.cachedEditTodo = Object.assign({}, todo);

        todo.editMode = true;
      }
    },
    cancelEdit(todo) {
      if (!(this.editedTodo === todo)) return;

      Object.assign(todo, this.cachedEditTodo);

      todo.editMode = false;

      this.editedTodo = null;
      this.cachedEditTodo = null;
    },
    setColor(todo, color) {
      todo.color = color;
    },
    updateTodo(todo) {
      todo.editMode = false;

      this.editedTodo = null;
      this.cacheEditTodo = null;
    },
    removeCompleted() {
      this.todos = this.todos.filter(item => !item.completed);
    },
    setDefault() {
      this.newTodo = '';
      this.selectedColor = '#ef5350';
      this.newTodoError = false;
    },
  },
  computed: {
    completedTodos() {
      return this.todos.filter(item => item.completed).length;
    },
  },
  directives: {
    ClickOutside,
    focus(el, binding) {
      setTimeout(() => {
        if (binding.value) {
          el.focus();
        }
      }, 0);
    },
  },
  mounted() {
    const todos = JSON.parse(localStorage.getItem(this.storage.key) || '[]');

    todos.forEach((todo, index) => {
      todo.id = index;
    });

    this.storage.uid = todos.length;

    this.todos = todos;
  },
};
